package fr.afpa.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.entity.Annonce;

public class DaoAnnonce {

//	DbConn conn = new DbConn();
//	
//
//	/**
//	 * Method for add annonce from database
//	 * 
//	 * @param annonce : the annonce
//	 * @param idUser  : the USER ID
//	 */
//	public void addAnnonce(Annonce annonce, int idUser) {
//
//		PreparedStatement stmt = null;
//
//		String query = "Insert into Annonce (titre_annonce , isbn ,  date_edition , maison_edition,"
//				+ " prix_untaire, quantite,remise, id_user) " + "	values(?,?,?,?,?,?,?,?)";
//
//		try {
//
//			stmt = conn.connect().prepareStatement(query);
//
//			stmt.setString(1, annonce.getTitre());
//			stmt.setString(2, annonce.getIsbn());
//			stmt.setDate(3, Date.valueOf(annonce.getDateEdition()));
//			stmt.setString(4, annonce.getMaisonEdition());
//			stmt.setDouble(5, annonce.getPrixUni());
//			stmt.setInt(6, annonce.getQuantite());
//			stmt.setDouble(7, annonce.getRemise());
//			stmt.setInt(8, idUser);
//
//			stmt.executeUpdate();
//
//		} catch (SQLException e) {
//
//			e.printStackTrace();
//		} finally {
//
//			if (stmt != null) {
//
//				System.out.println("Ajout effectu�");
//
//				try {
//
//					stmt.close();
//
//				} catch (SQLException e) {
//
//					e.printStackTrace();
//				}
//			}
//		}
//
//	}
//
//	/**
//	 * method to update an annonce
//	 * 
//	 * @param annonce : the annonce to update
//	 */
//	public void updateAnnonce(Annonce annonce) {
//
//		PreparedStatement stmt = null;
//
//		String query = "Update Annonce  set titre_annonce  = ? , isbn = ? ,  date_edition = ? , maison_edition = ?,"
//				+ " prix_untaire = ? , quantite = ? , remise = ? where id_annonce = ?";
//
//		try {
//
//			stmt = conn.connect().prepareStatement(query);
//
//			stmt.setString(1, annonce.getTitre());
//			stmt.setString(2, annonce.getIsbn());
//			stmt.setDate(3, Date.valueOf(annonce.getDateEdition()));
//			stmt.setString(4, annonce.getMaisonEdition());
//			stmt.setDouble(5, annonce.getPrixUni());
//			stmt.setInt(6, annonce.getQuantite());
//			stmt.setDouble(7, annonce.getRemise());
//			stmt.setInt(8, annonce.getIdAnnonce());
//
//			stmt.executeUpdate();
//
//		} catch (SQLException e) {
//
//			e.printStackTrace();
//		} finally {
//			if (stmt != null) {
//
//				try {
//					System.out.println("Modification effectu�e");
//					stmt.close();
//
//				} catch (SQLException e) {
//
//					e.printStackTrace();
//				}
//			}
//		}
//
//	}
//
//	/***
//	 * Method to delete an annonce
//	 * 
//	 * @param annonce : annonce to delete
//	 */
//	public void deleteAnnonce(Annonce annonce) {
//
//		PreparedStatement stmt = null;
//
//		String query = "Delete from Annonce where id_annonce = ? ";
//
//		try {
//
//			stmt = conn.connect().prepareStatement(query);
//			stmt.setInt(1, annonce.getIdAnnonce());
//
//			stmt.executeUpdate();
//
//		} catch (SQLException e) {
//
//			e.printStackTrace();
//		} finally {
//			if (stmt != null) {
//
//				try {
//
//					System.out.println("Suppression effectu�e");
//					stmt.close();
//
//				} catch (SQLException e) {
//
//					e.printStackTrace();
//				}
//			}
//		}
//
//	}
//
//	/**
//	 * Method for search a annonce by id and id user
//	 * 
//	 * @param idUser    : the user id
//	 * @param idAnnonce : the id annonce
//	 * @return a annonce object
//	 */
//	public Annonce findAnnonceByIdUserAndIDAnnonce(int idUser, int idAnnonce) {
//
//		PreparedStatement stmt = null;
//
//		Annonce annonce = null;
//
//		String query = "Select * from annonce where id_user = ? and id_annonce = ? ";
//
//		try {
//
//			stmt = conn.connect().prepareStatement(query);
//
//			stmt.setInt(1, idUser);
//			stmt.setInt(2, idAnnonce);
//
//			ResultSet res = stmt.executeQuery();
//
//			if (res.next()) {
//
//				annonce = new Annonce();
//				annonce.setIdAnnonce(res.getInt(1));
//				annonce.setTitre(res.getString(2));
//				annonce.setIsbn(res.getString(3));
//				annonce.setDateEdition(LocalDate.parse(res.getString(4)));
//				annonce.setPrixUni(res.getDouble(6));
//				annonce.setMaisonEdition(res.getString(5));
//				annonce.setQuantite(res.getInt(7));
//				annonce.setRemise(res.getDouble(8));
//				annonce.setDateAnnonce(LocalDate.parse(res.getString(9)));
//				annonce.setIdUser(res.getInt(10));
//
//			}
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} finally {
//			if (stmt != null) {
//				try {
//					stmt.close();
//				} catch (SQLException e) {
//
//					e.printStackTrace();
//				}
//			}
//		}
//
//		return annonce;
//	}
//
//	/**
//	 * Method to list all annonce(s)
//	 * 
//	 * @return list<Annonce> :the result list
//	 */
//	public List<Annonce> ListerAnnonce() {
//
//		ArrayList<Annonce> listAnnonce = new ArrayList<>();
//		PreparedStatement stmt = null;
//
//		String query = "Select * from Annonce";
//
//		try {
//
//			stmt = conn.connect().prepareStatement(query);
//			ResultSet res = stmt.executeQuery();
//
//			Annonce annonce = null;
//
//			while (res.next()) {
//
//				annonce = new Annonce();
//				annonce.setIdAnnonce(res.getInt(1));
//				annonce.setTitre(res.getString(2));
//				annonce.setIsbn(res.getString(3));
//				annonce.setDateEdition(LocalDate.parse(res.getString(4)));
//				annonce.setPrixUni(res.getDouble(6));
//				annonce.setMaisonEdition(res.getString(5));
//				annonce.setQuantite(res.getInt(7));
//				annonce.setRemise(res.getDouble(8));
//				annonce.setDateAnnonce(LocalDate.parse(res.getString(9)));
//				annonce.setIdUser(res.getInt(10));
//
//				listAnnonce.add(annonce);
//
//			}
//
//		} catch (SQLException e) {
//
//			e.printStackTrace();
//
//		} finally {
//
//			if (stmt != null) {
//
//				try {
//
//					stmt.close();
//
//				} catch (SQLException e) {
//
//					e.printStackTrace();
//				}
//			}
//		}
//
//		return listAnnonce;
//
//	}
//
//	/**
//	 * Method for search annonce by user id
//	 * 
//	 * @param idUser : the user id
//	 * @return list<Annonce> : the result of search
//	 */
//	public List<Annonce> listerAnnonceByUser(int idUser) {
//
//		ArrayList<Annonce> listAnnonce = new ArrayList<>();
//		PreparedStatement stmt = null;
//
//		String query = "Select * from annonce where id_user = ? ";
//
//		Annonce annonce = null;
//
//		try {
//
//			stmt = conn.connect().prepareStatement(query);
//			stmt.setInt(1, idUser);
//
//			ResultSet res = stmt.executeQuery();
//
//			while (res.next()) {
//
//				annonce = new Annonce();
//				annonce.setIdAnnonce(res.getInt(1));
//				annonce.setTitre(res.getString(2));
//				annonce.setIsbn(res.getString(3));
//				annonce.setDateEdition(LocalDate.parse(res.getString(4)));
//				annonce.setPrixUni(res.getDouble(6));
//				annonce.setMaisonEdition(res.getString(5));
//				annonce.setQuantite(res.getInt(7));
//				annonce.setRemise(res.getDouble(8));
//				annonce.setDateAnnonce(LocalDate.parse(res.getString(9)));
//				annonce.setIdUser(res.getInt(10));
//
//				listAnnonce.add(annonce);
//
//			}
//
//		} catch (SQLException e) {
//
//			e.printStackTrace();
//		} finally {
//
//			if (stmt != null) {
//
//				try {
//				
//					stmt.close();
//
//				} catch (SQLException e) {
//
//					e.printStackTrace();
//				}
//			}
//		}
//
//		return listAnnonce;
//
//	}
//
//	/**
//	 * Method for search annonce by key word in the title
//	 * 
//	 * @param str : the key word
//	 * @return list<Annonce> : the result list
//	 */
//	public List<Annonce> listAnnonceByMotcle(String str) {
//
//		ArrayList<Annonce> listAnnonce = new ArrayList<>();
//		PreparedStatement stmt = null;
//
//		String query = "Select *  from annonce where titre_annonce like ('%" + str + "%')";
//		Annonce annonce = null;
//
//		try {
//
//			stmt = conn.connect().prepareStatement(query);
//
//			ResultSet res = stmt.executeQuery();
//
//			while (res.next()) {
//
//				annonce = new Annonce();
//				annonce.setIdAnnonce(res.getInt(1));
//				annonce.setTitre(res.getString(2));
//				annonce.setIsbn(res.getString(3));
//				annonce.setDateEdition(LocalDate.parse(res.getString(4)));
//				annonce.setPrixUni(res.getDouble(6));
//				annonce.setMaisonEdition(res.getString(5));
//				annonce.setQuantite(res.getInt(7));
//				annonce.setRemise(res.getDouble(8));
//				annonce.setDateAnnonce(LocalDate.parse(res.getString(9)));
//				annonce.setIdUser(res.getInt(10));
//
//				listAnnonce.add(annonce);
//
//			}
//		} catch (SQLException e) {
//
//			e.printStackTrace();
//		} finally {
//
//			if (stmt != null) {
//
//				try {
//
//					stmt.close();
//
//				} catch (SQLException e) {
//
//					e.printStackTrace();
//				}
//			}
//		}
//
//		return listAnnonce;
//
//	}
//
//	/**
//	 * Method for search annonce by isbn
//	 * 
//	 * @param isbn string : the isbn
//	 * @return list<Annonce> : the result list
//	 */
//	public List<Annonce> listAnnonceByISBN(String isbn) {
//
//		ArrayList<Annonce> listAnnonce = new ArrayList<>();
//		PreparedStatement stmt = null;
//
//		String query = "Select *  from annonce where isbn = ?";
//		Annonce annonce = null;
//
//		try {
//
//			stmt = conn.connect().prepareStatement(query);
//			stmt.setString(1, isbn);
//			ResultSet res = stmt.executeQuery();
//
//			while (res.next()) {
//
//				annonce = new Annonce();
//				annonce.setIdAnnonce(res.getInt(1));
//				annonce.setTitre(res.getString(2));
//				annonce.setIsbn(res.getString(3));
//				annonce.setDateEdition(LocalDate.parse(res.getString(4)));
//				annonce.setPrixUni(res.getDouble(6));
//				annonce.setMaisonEdition(res.getString(5));
//				annonce.setQuantite(res.getInt(7));
//				annonce.setRemise(res.getDouble(8));
//				annonce.setDateAnnonce(LocalDate.parse(res.getString(9)));
//				annonce.setIdUser(res.getInt(10));
//
//				listAnnonce.add(annonce);
//
//			}
//		} catch (SQLException e) {
//
//			e.printStackTrace();
//		} finally {
//
//			if (stmt != null) {
//
//				try {
//
//					stmt.close();
//
//				} catch (SQLException e) {
//
//					e.printStackTrace();
//				}
//			}
//		}
//
//		return listAnnonce;
//
//	}
//
//	/**
//	 * Method for search annonce by ville
//	 * 
//	 * @param ville string : the ville
//	 * @return list<Annonce> : the result list
//	 */
//	public List<Annonce> listAnnonceByVille(String ville) {
//		ArrayList<Annonce> listAnnonce = new ArrayList<>();
//		PreparedStatement stmt = null;
//
//		String query = "select * from annonce a , users u  " + "	where a.id_user = u.id_user "
//				+ "	and u.id_adresse  in" + "	(select  id_adresse from adresse a where ville= ? ) ;";
//
//		Annonce annonce = null;
//
//		try {
//
//			stmt = conn.connect().prepareStatement(query);
//			stmt.setString(1, ville);
//			ResultSet res = stmt.executeQuery();
//
//			while (res.next()) {
//
//				annonce = new Annonce();
//				annonce.setIdAnnonce(res.getInt(1));
//				annonce.setTitre(res.getString(2));
//				annonce.setIsbn(res.getString(3));
//				annonce.setDateEdition(LocalDate.parse(res.getString(4)));
//				annonce.setPrixUni(res.getDouble(6));
//				annonce.setMaisonEdition(res.getString(5));
//				annonce.setQuantite(res.getInt(7));
//				annonce.setRemise(res.getDouble(8));
//				annonce.setDateAnnonce(LocalDate.parse(res.getString(9)));
//				annonce.setIdUser(res.getInt(10));
//
//				listAnnonce.add(annonce);
//
//			}
//		} catch (SQLException e) {
//
//			e.printStackTrace();
//		} finally {
//
//			if (stmt != null) {
//
//				try {
//
//					stmt.close();
//
//				} catch (SQLException e) {
//
//					e.printStackTrace();
//				}
//			}
//		}
//
//		return listAnnonce;
//	}

}