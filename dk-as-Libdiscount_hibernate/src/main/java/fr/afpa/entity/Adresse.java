package fr.afpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@Setter
@Getter
@RequiredArgsConstructor 
@NoArgsConstructor
@Entity
public class Adresse {
@Id

@GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
@NonNull
	private String numRue;
@NonNull
	private String nomRue;
@NonNull
	private String codeP;
@NonNull
	private String ville;

	public String toString() {
		return "\n           num�ro de rue  : " + numRue + "\n           nom de rue  : " + nomRue
				+ "\n           code postal : " + codeP + "\n           ville : " + ville;

	}
}

