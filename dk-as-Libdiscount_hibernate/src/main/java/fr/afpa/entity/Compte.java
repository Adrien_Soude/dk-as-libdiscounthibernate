package fr.afpa.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@Setter
@Getter
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Compte {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
@Column(name = "fk_compte")
    private int id;
@NonNull
	private String login;
@NonNull
	private String mdp;

	public String toString() {
		return "           Votre login  : " + login + "\n           nom  : " + mdp;
	}
}
