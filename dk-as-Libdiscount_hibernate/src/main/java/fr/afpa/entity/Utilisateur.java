package fr.afpa.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;



import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
public class Utilisateur {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Utilsateur")
	private int id;
	@NonNull
	private String nom;
	@NonNull
	private String prenom;
	@NonNull
	private String mail;
	@NonNull
	private String nomLib;
	@NonNull
	private String tel;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name ="fk_adresse" )
	private Adresse adresse;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name ="fk_compte")
	private Compte compte;
    //private List<Annonce>listAnnonces;
	@Override
	public String toString() {
		return "\n         nom  : " + nom + "\n           prenom : " + prenom
			+ "\n           mail : " + mail + "\n           librairie : " + nomLib + "\n           tel : " + tel;
	}
}
