package fr.afpa.control;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * 
 * @author Adrien SOUDE && Fouad BOUCHLAGHEM
 *
 */
public class ControlSaisie {

	public boolean controlMail(String mail) {

		if (!(mail.contains("@")) || !(mail.contains("."))) {

			return false;

		}

		return true;
	}

	public boolean controlCodeP(String codeP) {

		if (isNumeric(codeP) == true && codeP.length() == 5) {

			return true;

		} else {

			return false;
		}
	}

	public boolean controlTel(String tel) {

		if (isNumeric(tel) == true && tel.length() == 10) {

			return true;

		} else {

			return false;
		}
	}

	/**
	 * Method for check if a element of list of string is empty
	 * 
	 * @param liststr : a list of string
	 * @return boolean : false if a element is empty , true if no element are empty
	 */
	public boolean isEmptys(List<String> listSrt) {

		for (String string : listSrt) {

			if (string.isEmpty()) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Method for check if a string has the good format of date
	 * 
	 * @param date : the string we are checking
	 * @return boolean : true if the date is valid , false if the date have wrong
	 *         format
	 */
	public boolean isDate(String date) {

		if (date.matches("([0-2][0-9]|(3)[0-1])(-)(((0)[0-9])|((1)[0-2]))(-)\\d{4}")) {
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

			try {

				format.parse(date);

				return true;

			} catch (ParseException e) {
				return false;
			}
		}

		return false;

	}

	/**
	 * Method for check if a string is numeric
	 * 
	 * @param str : the string we are checking
	 * @return boolean : true if the str is all numeric, false if the str it's not
	 *         numeric
	 */
	public boolean isNumeric(String str) {

		boolean isNum = false;

		for (Character c : str.toCharArray()) {

			if (Character.isDigit(c)) {

				isNum = true;

			} else {

				isNum = false;

			}
		}
		return isNum;

	}

	/**
	 * Method for check if a double is positive
	 * 
	 * @param db : double
	 * @return boolean : true if the is valid or false if is not valid
	 */
	public boolean checkPostiveDouble(double db) {

		if (db < 0) {

			return false;

		}

		return true;

	}

	/**
	 * Methode for check if int is positive
	 * 
	 * @param number : int
	 * @return boolean : true if the is valide or false if is not valide
	 */
	public boolean checkPostiveInt(int number) {
		if (number < 0) {
			return false;
		}
		return true;
	}

}