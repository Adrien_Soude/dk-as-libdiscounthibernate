package fr.afpa.vue;



import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import fr.afpa.entity.Annonce;
import fr.afpa.control.ControlAnnonce;
import fr.afpa.control.ControlSaisie;
import fr.afpa.control.ControlUser;


 

public class AffichageMenu {

	private ControlAnnonce ca = new ControlAnnonce();
	private ControlSaisie cs = new ControlSaisie();
	private ControlUser cu = new ControlUser();

	Scanner scan = new Scanner(System.in);
	private static final String wrong = " Choix invalide, veuillez choisir une option valide ";

	private String demandeNom;
	private String demandePrenom;
	private String demandeMail;
	private String demandeTel;

	Scanner in = new Scanner(System.in);

	/**
	 * Method for print the start Menu
	 */
	public void startMenu() {

		System.out.println("-----------------Bienvenu----------------");
		System.out.println("-       Tapez 1 pour vous inscrire      -");
		System.out.println("-                                       -");
		System.out.println("-       Tapez 2 pour vous connecter     -");
		System.out.println("-                                       -");
		System.out.println("-       Tapez 3 pour quitter            -");
		System.out.println("-----------------------------------------");

		startMenu(scan);

		
		
	}
public  void ajouterUtilisateur(Scanner can) {
 String nom;
 String prenom;
 String mail;
 String nomLib;
 String ville;
 String tel;
 String numRue;
 String nomRue;
 String codeP;
 String login;
 String mdp;
  can.nextLine();
 System.out.println("--------------------------------- -----Veillez entrez votre nom---------------------------------------------------------");
 nom =can.nextLine();
 System.out.println("-------------------------------------- Veillez entrez votre prenom---------------------------------------------------------");
 prenom =can.nextLine();
 System.out.println("-------------------------------------- Veillez entrez votre mail---------------------------------------------------------");
 mail=can.nextLine();
 System.out.println("-------------------------------------- Veillez entrez votre nomLib---------------------------------------------------------");
 nomLib=can.nextLine();
 System.out.println("-------------------------------------- Veillez entrez votre tel---------------------------------------------------------");
 tel =can.nextLine();
 System.out.println("-------------------------------------- Veillez entrez votre numRue---------------------------------------------------------");
 numRue =can.nextLine();
 System.out.println("-------------------------------------- Veillez entrez votre nomRue---------------------------------------------------------");
 nomRue =can.nextLine();
 System.out.println("---------------- ----------------------Veillez entrez votre codeP---------------------------------------------------------");
 codeP =can.nextLine();
 System.out.println("---------------- ----------------------Veillez entrez votre ville---------------------------------------------------------");
 ville =can.nextLine();
 System.out.println("--------------------------------------Veillez entrez votrelogin---------------------------------------------------------");
 login =can.nextLine();
 System.out.println("--------------------------------------Veillez entrez votre mdp---------------------------------------------------------");
 mdp =can.nextLine();
 
  cu.ajouterUtilisateur(nom, prenom, mail, nomLib, ville, tel, numRue, nomRue, codeP, login, mdp);
}
	/**
	 * Methode for print the add annonce Menu
	 */
	public void addAnnonce(int idUser) {

		System.out.println("-------------- Ajouter Votre Annonce ----------------");
		addAnnonce(scan, idUser);

	}

	/**
	 * Method for print the authification menu
	 */
	public void authMenu() {

		System.out.println("----------------- Espace Connection -----------------");
		System.out.println("-              Tapez -1 pour quitter                -");
		authMenu(scan);

	}

	/**
	 * Affichage du menu principal
	 * 
	 * @param userID
	 */
	public void mainMenu(int userID) {

		System.out.println("------------------  Menu Principal  -----------------");
		System.out.println("-        Tapez 1 pour voir les annonces             -");
		System.out.println("-        Tapez 2 pour rechercher des annonces       -");
		System.out.println("-        Tapez 3 pour voir vos annonces             -");
		System.out.println("-        Tapez 4 pour ajouter une annoces           -");
		System.out.println("-        Tapez 5 pour consulter vos infos           -");
		System.out.println("-        Tapez 6 pour modifier vos infos            -");
		System.out.println("-        Tapez 7 pour desactiver votre compte       -");
		System.out.println("-        Tapez 8 pour quitter                       -");
		System.out.println("-----------------------------------------------------");
		gestionMainMenu(scan, userID);
	}

	/**
	 * affichage du menu de rechercher
	 * 
	 * @param userID
	 */
	public void searchMenu(int userID) {

		System.out.println("-------------- Menu de rechercher des annonces ----------------");
		System.out.println("-          Tapez 1 pour rechecher une annonce par ISBN         ");
		System.out.println("-          Tapez 2 pour rechecher une annonce par Mots cl�s    ");
		System.out.println("-          Tapez 3 pour rechecher une annonce par Ville        ");
		System.out.println("-          Tapez 4 pour retourner au menu principal            ");
		System.out.println("-          Tapez 5 pour quitter                                ");
		System.out.println("---------------------------------------------------------------");
		searchMenu(scan, userID);
	}

	/**
	 * affichage du menu annonce
	 * 
	 * @param userID
	 */
	public void dashbordAnnonce(int userID) {

		System.out.println("-------------- tableau de bord de vos annonces ----------------");
		System.out.println("-          Tapez 1 pour modifier une annonce                  -");
		System.out.println("-          Tapez 2 pour supprimer une annonce                 -");
		System.out.println("-          Tapez 3 pour retourner au menu principal           -");
		System.out.println("-          Tapez 4 pour quitter                               -");
		System.out.println("---------------------------------------------------------------");
		dashbordAnnonce(userID, scan);

	}

	/**
	 * methode de gestion du menu de connexion
	 * 
	 * @param scan
	 */
	public void startMenu(Scanner scan) {

		int choix = 0;

		while (choix != 3) {

			choix = scan.nextInt();

			switch (choix) {

			case 1:
				ajouterUtilisateur(scan);
				startMenu();
				break;
			case 2:
				authMenu();
				break;
			case 3:
				System.exit(0);
				break;
			default:
				System.out.println(wrong);
				break;
			}

		}

	}

	/**
	 * gestion de main menu
	 * 
	 * @param scan
	 * @param idUser
	 */
	private void gestionMainMenu(Scanner scan, int idUser) {
		int choix = 0;

		while (choix != 8) {

			choix = scan.nextInt();

			switch (choix) {

			case 1:
				ca.listAnnonce();
				mainMenu(idUser);
				break;
			case 2:
				searchMenu(idUser);
				break;
			case 3:
				ca.listAnnonceByUser(idUser);
				dashbordAnnonce(idUser);
				break;
			case 4:
				addAnnonce(idUser);
				break;
			case 5:
				voirInfos(idUser);
				break;
			case 6:
				demandeModif(idUser);
				break;
			case 7:
				desactiverUser(idUser);
				break;
			case 8:
				System.exit(0);
				break;
			default:
				System.out.println(wrong);
				break;
			}

		}
	}

	/**
	 * Form for add annonce
	 * 
	 * @param scan : a Scanner;
	 */
	private void addAnnonce(Scanner scan, int idUser) {

		int choix = 0;
		String titre;
		String isbn;
		String maisonE;
		String dateE;
		double prixUni;
		double remise;
		int quantite;

		scan.nextLine();

		do {

			System.out.println("-  Veuillez entrer le titre de votre annonce        -");
			titre = scan.nextLine();

		} while (titre.isEmpty());

		do {

			System.out.println("-  Veuillez entrer l'ISBN du livre � vendre         -");
			isbn = scan.nextLine();

		} while (isbn.isEmpty() || !cs.isNumeric(isbn));

		do {

			System.out.println("-  Veuillez entrer La maison d'�dition du livre     -");
			maisonE = scan.nextLine();

		} while (maisonE.isEmpty());

		do {

			System.out.println("-  Veuillez entrer le prix unitaire du livre        -");
			prixUni = scan.nextDouble();
			scan.nextLine();

		} while (!cs.checkPostiveDouble(prixUni));

		do {

			System.out.println("-  Veuillez entrer la date d'�dition (jj-MM-aaaa)   -");
			dateE = scan.nextLine();

		} while (!dateE.isEmpty() && !cs.isDate(dateE));

		do {

			System.out.println("-  Veuillez entrer la quantite de livres            -");
			quantite = scan.nextInt();
			scan.nextLine();

		} while (!cs.checkPostiveInt(quantite));

		do {

			System.out.println("-  Veuillez entrer la remise sur le prix original   -");
			remise = scan.nextDouble();
			scan.nextLine();

		} while (!cs.checkPostiveDouble(remise));

		ca.addAnnonce(titre, isbn, maisonE, prixUni, quantite, remise, dateE, idUser);

		do {

			System.out.println("-            Tapez -1 pour quitter                  -");
			choix = scan.nextInt();
			scan.nextLine();

		} while (choix != -1);

		mainMenu(idUser);

	}

	/**
	 * gestion du menu auth
	 * 
	 * @param scan
	 */

	private String login = "";

	public void authMenu(Scanner scan) {

		String mdp = "";

		scan.nextLine();
		do {

			System.out.println("-              Veuillez entrer votre login          -");
			login = scan.nextLine();

			if (login.equals("-1")) {
				System.out.println("-----------------------------------------------------");
				startMenu();
			}

		} while (login.isEmpty());

		do {

			System.out.println("-              Veuillez entrer votre mot de passe   -");
			mdp = scan.nextLine();

			if (mdp.equals("-1")) {
				System.out.println("-----------------------------------------------------");
				startMenu();
			}

		} while (mdp.isEmpty());

		//if (cu.auth(login, mdp) > 0) {
			System.out.println("-----------------------------------------------------");
	//		mainMenu(cu.auth(login, mdp));

		//} else if (cu.auth(login, mdp) == -2) {
			System.out.println("Votre compte est d�sactiver..");
			System.out.println("-----------------------------------------------------");
			startMenu();
	//	} else {
			System.out.println("Mot de passe ou login incorrect ");
			System.out.println("-----------------------------------------------------");
//			authMenu();
	//	}
	}

	/**
	 * gestion du menu searchMenu
	 * 
	 * @param scan
	 * @param userID
	 */
	private void searchMenu(Scanner scan, int userID) {

		int choix = 0;
		String str;
		final String retour = "Tapez 4 pour retourner au menu principal ";

		while (choix != 4) {

			choix = scan.nextInt();
			scan.nextLine();

			switch (choix) {

			case 1:
				System.out.println("Entrez l'isbn ");
				str = scan.nextLine();
				ca.listAnnonceByISBN(str);
				System.out.println(retour);
				choix = scan.nextInt();
				scan.nextLine();
				searchMenu(userID);
				break;
			case 2:
				System.out.println("Entrez le(s) mot(s) cl�(s) ");
				str = scan.nextLine();
				ca.listAnnonceByMotCle(str);
				System.out.println(retour);
				choix = scan.nextInt();
				scan.nextLine();
				searchMenu(userID);
				break;
			case 3:
				System.out.println("Entrez la ville ");
				str = scan.nextLine();
				ca.listAnnonceByVille(str);
				System.out.println(retour);
				choix = scan.nextInt();
				scan.nextLine();
				searchMenu(userID);
				break;
			case 4:

				break;
			case 5:
				System.exit(0);
				break;
			default:
				System.out.println(wrong);
				break;
			}

		}
		mainMenu(userID);
	}

	public void demandeModif(int idUser) {

		System.out.print("Quel est votre nouveau nom ? : ");
		demandeNom = in.nextLine();

		System.out.print("Quel est votre nouveau prenom ? : ");

		demandePrenom = in.nextLine();

		System.out.print("Quel est votre nouveau mail ? : ");
		demandeMail = in.nextLine();

		while (cs.controlMail(demandeMail) != true) {

			System.out.print("Saisissez un mail contenant un '@' et un '.' ! : ");
			demandeMail = in.nextLine();

		}

		System.out.print("Quel est votre nouveau num�ro de t�l ? : ");
		demandeTel = in.nextLine();

		while (cs.controlTel(demandeTel) != true)

		{

			System.out.print("Saisissez un t�l�phone contenant 10 chiffres ! : ");
			demandeTel = in.nextLine();

		}

	//	cu.demandeModif(idUser, demandeNom, demandePrenom, demandeMail, demandeTel);
	}

	/**
	 * gestion du menu dashbordAnnonce
	 * 
	 * @param userID
	 * @param scan
	 */
	private void dashbordAnnonce(int userID, Scanner scan) {
		int choix = 0;
		int id = 0;

		while (choix != 3) {

			choix = scan.nextInt();
			scan.nextLine();

			switch (choix) {

			case 1:

				System.out.println("Veuillez entrer le numero de l'annonce a modifier ");
				id = scan.nextInt();

				if (ca.searchAnnonceByIdUserAndIdAnnonce(userID, id) != null) {

					formUpdateAnnonce(userID, id);

				} else {

					System.out.println("Annonce introuvable, veuillez v�rifier le num�ro de l'annonce");
					dashbordAnnonce(userID);
				}

				break;
			case 2:

				System.out.println("Veuillez entrer le numero de l'annonce a supprimer ");
				id = scan.nextInt();

				if (ca.searchAnnonceByIdUserAndIdAnnonce(userID, id) != null) {

					ca.supprimerAnnoce(ca.searchAnnonceByIdUserAndIdAnnonce(userID, id));
					dashbordAnnonce(userID);

				} else {

					System.out.println("Annonce introuvable, veuillez v�rifier le num�ro de l'annonce");
					dashbordAnnonce(userID);
				}

				break;
			case 3:
				mainMenu(userID);
				break;
			case 4:
				System.exit(0);
				break;
			default:
				System.out.println(wrong);
				break;
			}
		}
		mainMenu(userID);

	}

	/**
	 * Formulaire pour l'update de l'annonce
	 * 
	 * @param userID : l'id utilisateur de l'annonce
	 * @param id     : l'id de annonce
	 */
	public void formUpdateAnnonce(int userID, int id) {

		int choixForm;
		Annonce annonce = ca.searchAnnonceByIdUserAndIdAnnonce(userID, id);

		String titre = null;
		String isbn;
		String maisonE;
		String dateE;
		double prixUni;
		double remise;
		int quantite;

		scan.nextLine();

		do {

			System.out.println("Le titre actuelle est : " + annonce.getTitre());
			System.out.println(" *Tapez 1  pour modifier ");
			System.out.println(" *Tapez 2  pour passer au champ suivant ");
			choixForm = scan.nextInt();
			scan.nextLine();

			if (choixForm == 1) {

				do {

					System.out.println("Veuillez entrer le nouveau titre : ");
					titre = scan.nextLine();

				} while (titre.isEmpty());

				annonce.setTitre(titre);

			} else if (choixForm != 2) {

				System.out.println(wrong);
			}

		} while (choixForm != 2);

		do {

			System.out.println("L'isbn actuelle est : " + annonce.getIsbn());
			System.out.println(" *Tapez 1  pour modifier ");
			System.out.println(" *Tapez 2  pour passer au champ suivant ");
			choixForm = scan.nextInt();
			scan.nextLine();

			if (choixForm == 1) {

				do {

					System.out.println("Veuillez entrer le nouvel isbn : ");
					isbn = scan.nextLine();

				} while (isbn.isEmpty() || !cs.isNumeric(isbn));

				annonce.setIsbn(isbn);

			} else if (choixForm != 2) {

				System.out.println(wrong);
			}

		} while (choixForm != 2);

		do {

			System.out.println("La Maison d'�dition actuelle est : " + annonce.getMaisonEdition());
			System.out.println(" *Tapez 1  pour modifier ");
			System.out.println(" *Tapez 2  pour passer au champ suivant ");
			choixForm = scan.nextInt();
			scan.nextLine();

			if (choixForm == 1) {

				do {

					System.out.println("Veuillez entrer la nouvelle maison d'�dition  : ");
					maisonE = scan.nextLine();

				} while (maisonE.isEmpty());

				annonce.setMaisonEdition(maisonE);

			} else if (choixForm != 2) {

				System.out.println(wrong);
			}

		} while (choixForm != 2);

		do {

			System.out.println("Le prix unitaire acutelle est de  : " + annonce.getPrixUni() + " � ");
			System.out.println(" *Tapez 1  pour modifier ");
			System.out.println(" *Tapez 2  pour passer au champ suivant ");
			choixForm = scan.nextInt();
			scan.nextLine();

			if (choixForm == 1) {

				do {

					System.out.println("Veuillez entrer le nouveau prix unitaire : ");
					prixUni = scan.nextDouble();
					scan.nextLine();

				} while (!cs.checkPostiveDouble(prixUni));

				annonce.setPrixUni(prixUni);

			} else if (choixForm != 2) {

				System.out.println(wrong);
			}

		} while (choixForm != 2);

		do {

			System.out.println("La date d'�dition actuelle est   : " + annonce.getDateEdition() + "");
			System.out.println(" *Tapez 1  pour modifier ");
			System.out.println(" *Tapez 2  pour passer au champ suivant ");
			choixForm = scan.nextInt();
			scan.nextLine();

			if (choixForm == 1) {

				do {

					System.out.println("Veuillez entrer la nouvelle date d'�dition (jj-MM-aaaa)  : ");
					dateE = scan.nextLine();

				} while (!dateE.isEmpty() && !cs.isDate(dateE));

				annonce.setDateEdition(LocalDate.parse(dateE, DateTimeFormatter.ofPattern("dd-MM-yyyy")));

			} else if (choixForm != 2) {

				System.out.println(wrong);
			}

		} while (choixForm != 2);

		do {

			System.out.println("La quantite actuelle est de : " + annonce.getQuantite());
			System.out.println(" *Tapez 1  pour modifier ");
			System.out.println(" *Tapez 2  pour passer au champ suivant ");
			choixForm = scan.nextInt();
			scan.nextLine();

			if (choixForm == 1) {

				do {

					System.out.println("Veuillez entrer la nouvelle quantite  : ");
					quantite = scan.nextInt();
					scan.nextLine();

				} while (!cs.checkPostiveInt(quantite));

				annonce.setQuantite(quantite);

			} else if (choixForm != 2) {

				System.out.println(wrong);
			}

		} while (choixForm != 2);

		do {

			System.out.println("La remise actuelle est de: " + annonce.getRemise());
			System.out.println(" *Tapez 1  pour modifier ");
			System.out.println(" *Tapez 2  pour passer au champ suivant ");
			choixForm = scan.nextInt();
			scan.nextLine();

			if (choixForm == 1) {

				do {

					System.out.println("Veuillez entrer la nouvelle remise : ");
					remise = scan.nextDouble();
					scan.nextLine();

				} while (!cs.checkPostiveDouble(remise));

				annonce.setRemise(remise);

			} else if (choixForm != 2) {

				System.out.println(wrong);
			}

		} while (choixForm != 2);

		ca.modifierAnnoce(annonce);
		dashbordAnnonce(userID);
	}

	public void desactiverUser(int idUser) {

		String choice = "";

		System.out.println("Vous �tes sur le point de d�sactiver votre compte ! �tes-vous s�r ?");
		System.out.println("Tapez 'O' pour oui");
		System.out.println("Tapez 'N' pour non");
		System.out.print("Votre d�cision finale : ");
		choice = in.nextLine();
		choice.toUpperCase();

		while (!choice.equals("O") && !choice.equals("N")) {

			System.out.print("Le choix est invalide ! : ");
			choice = in.nextLine();
			choice.toUpperCase();
		}

		if (choice.equals("O")) {

		//	cu.disabledUser(idUser);

		} else if (choice.equals("N")) {

			System.out.println("Sage d�cision :)");
			mainMenu(idUser);
		}
	}

	public void voirInfos(int idUser) {

		System.out.println("-------------- Voici vos informations ----------------");
	//	cu.voirInfos(idUser);
		
	}
}
